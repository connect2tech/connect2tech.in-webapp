<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Core Tag Example</title>
</head>
<body>

	<c:set var="income" value="1000" />
	<c:out value="${income}" />

	<c:choose>
		<c:when test="${income <= 1000}">
			<input type=text name=n1 value=n1>
		</c:when>
		<c:when test="${income > 1000}">
			<input type=text name=n2 value=n2>
		</c:when>
		<c:otherwise>  
     	  Income is undetermined...  
    	</c:otherwise>
	</c:choose>

</body>
</html>
