package com.c2t.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.*;

public class MyCookieServlet1 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			
			Cookie cook = new Cookie("course","java j2ee");
			Cookie cook2 = new Cookie("course2","java j2ee2");
			Cookie cook3 = new Cookie("course3","java j2ee3");
			
			
			response.addCookie(cook);
			response.addCookie(cook2);
			response.addCookie(cook3);
			
			out.print("Cookies setting done...");
			
			/*RequestDispatcher rd=request.getRequestDispatcher("welcome");
			rd.forward(request, response);*/
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}