package com.c2t.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WelcomeServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		

		Cookie ck[] = request.getCookies();

		for (int i = 0; i < ck.length; i++) {
			out.println("Cookie Name:: " + ck[i].getName());
			out.println("Cookie Value:: " + ck[i].getValue());
			out.print(ck[i].getPath());
		}
	}

}
