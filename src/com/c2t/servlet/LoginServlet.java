package com.c2t.servlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.*;


public class LoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		PrintWriter pw = response.getWriter();
		
		String fname = request.getParameter("fname");
		String pwd = request.getParameter("password");
		
		//when user is checking out, see if not logged in, then redirect to login page.
		if(fname.equals("chetan")){
			 RequestDispatcher rd1 = request.getRequestDispatcher("files/file1.html");
			 rd1.forward(request, response);
		}else{
			 //redirect to payment page.
			 RequestDispatcher rd2 = request.getRequestDispatcher("Page2.html");
			 rd2.forward(request, response);
		}
		
	}

}
