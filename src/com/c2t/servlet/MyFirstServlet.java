package com.c2t.servlet;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyFirstServlet extends HttpServlet {

	public void init() throws ServletException {
		System.out.println("I am inside init...");
		
		throw new ServletException("Error occured during init....");
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
		PrintWriter out = resp.getWriter();
		out.print("Hello, date and time is ::" + new Date());
	}

	public void method() {

	}
}
