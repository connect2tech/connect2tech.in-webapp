package com.c2t.servlet;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {
	
	public void init(){
		System.out.println("I am inside init...");
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
		PrintWriter pw = resp.getWriter();
		pw.print("Hello, date and time is ::"+new Date());
	}
}
