package com.c2t.servlet;

import java.text.SimpleDateFormat;
import java.io.*;
import java.util.Properties;
import java.net.URL;

import javax.servlet.*;

public class MyFilter implements Filter {

	public void init(FilterConfig arg0) throws ServletException {
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {

		System.out.println("I am filter....");

		chain.doFilter(req, resp);// sends request to next resource
		
		//Logic to authenticat the user....
		//

		// out.print("filter is invoked after");
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	
}