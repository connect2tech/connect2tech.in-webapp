package com.c2t.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class RedirectServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.sendRedirect("https://www.google.co.in/?gfe_rd=cr&dcr=0&ei=DSJ3WoL6Oe6G8QeUj77YBQ");
	}
}