package com.c2t.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SessionServlet2 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			HttpSession session = request.getSession();
			String name = (String)session.getAttribute("name");
			
			out.println("name="+name);
			out.println("Session Id="+session.getId());

			out.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}