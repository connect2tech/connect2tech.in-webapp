package com.c2t.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SessionServlet1 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			String n = request.getParameter("userName");

			HttpSession session = request.getSession();
			
			session.setAttribute("name", n);
			
			out.println("Session Id="+session.getId());

			out.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}